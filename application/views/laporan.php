<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Laporan</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script
  src="http://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<button class="btn btn-primary" data-toggle="modal" data-target="#tambahModal">Tambah</button>
		<a href="<?=site_url().'/proses/cetak/'.$tipe?>" class="btn btn-warning" target="_blank">Cetak</a>
		<div class="row">
			<div class="col-md-12">
				<table>
					<thead>
						<tr>
							<th>No</th>
							<th>Tanggal</th>
							<th>Nama</th>
							<th>Jumlah</th>
							<th></th>
						</tr>
					</thead>
					<tfoot>
						<?php
						foreach ($all_data as $key => $value) {
							?>
							<tr>
								<td><?=$key+1?></td>
								<td><?=$value->tanggal?></td>
								<td><?=$value->nama?></td>
								<td><?=$value->jumlah?></td>
								<td><button class="btn btn-primary" data-toggle="modal" data-target="#editModal" data-id="<?=$value->id?>">Edit</button><a href="<?=site_url().'/proses/hapus/'.$tipe.'/'.$value->id?>" class="btn btn-danger">Delete</a></td>
							</tr>
							<?php
						}
						?>
					</tfoot>
				</table>
			</div>
		</div>
	</div>

	<div class="modal fade" id="tambahModal" role="dialog" aria-labelledby="cu_label" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header modal-header-primary">
					<h5 class="modal-title" id="cu_label">Tambah</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<?=form_open('proses/tambah/'.$tipe)?>
				<div class="modal-body">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Tanggal</label>
						<div class="col-sm-10">
							<input type="date" name="tanggal" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Nama</label>
						<div class="col-sm-10">
							<input type="text" name="nama" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Jumlah</label>
						<div class="col-sm-10">
							<input type="number" name="jumlah" class="form-control">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary">Tambah</button>
				</div>
				<?php echo form_close()?>
			</div>
		</div>
	</div>

	<div class="modal fade" id="editModal" role="dialog" aria-labelledby="cu_label" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header modal-header-primary">
					<h5 class="modal-title" id="cu_label">Edit</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<?=form_open('proses/edit/lin1')?>
				<input type="hidden" name="id">
				<div class="modal-body">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Tanggal</label>
						<div class="col-sm-10">
							<input type="date" name="tanggal" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Nama</label>
						<div class="col-sm-10">
							<input type="text" name="nama" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Jumlah</label>
						<div class="col-sm-10">
							<input type="number" name="jumlah" class="form-control">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary">Edit</button>
				</div>
				<?php echo form_close()?>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$('#editModal').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget)
			var id = button.data('id')
			var modal = $(this)
			$.ajax({
				"url": '<?=site_url().'/proses/ajax_edit/'.$tipe.'/'?>'+id,
				"method": "GET"
			}).done(function (res) {
				json = JSON.parse(res)
				console.log(json)
				$.each( json, function( key, value ) {
					modal.find('[name="'+key+'"]').val(value)
				});
			})
		})
	</script>
</body>
</html>