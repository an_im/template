<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proses extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('proses_model');
	}

	function tambah($tabel)
	{
		$insert_data = [
			'tanggal' => $this->input->post('tanggal'),
			'nama' => $this->input->post('nama'),
			'jumlah' => $this->input->post('jumlah')
		];
		$this->proses_model->tambah($tabel, $insert_data);
		redirect('laporan/tipe/'.$tabel);
	}

	function cetak($tabel)
	{
		$data['data'] = $this->proses_model->get_data($tabel);
		$this->load->view($tabel, $data);
	}

	function hapus($tabel, $id)
	{
		$this->proses_model->hapus($tabel, $id);
		redirect('laporan/tipe/'.$tabel);
	}

	function ajax_edit($tabel, $id)
	{
		$data = $this->proses_model->get_single_data($tabel, $id);
		echo json_encode($data);
	}

	function edit($tabel)
	{
		$id = $this->input->post('id');
		$update_data = [
			'tanggal' => $this->input->post('tanggal'),
			'nama' => $this->input->post('nama'),
			'jumlah' => $this->input->post('jumlah')
		];
		$this->proses_model->edit($tabel, $update_data, $id);
		redirect('laporan/tipe/'.$tabel);
	}
}
