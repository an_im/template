<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('laporan_model');
	}

	function tipe($tabel)
	{
		$data['tipe'] = $tabel;
		$data['all_data'] = $this->laporan_model->get_all_data($tabel);
		$this->load->view('laporan', $data);
	}
}
