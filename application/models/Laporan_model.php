<?php
class Laporan_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_all_data($tabel)
	{
		$query = $this->db->get($tabel);
		return $query->result();
	}
}