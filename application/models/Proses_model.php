<?php
class Proses_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function tambah($tabel, $data)
	{
		$this->db->insert($tabel, $data);
	}

	function get_data($tabel)
	{
		$query = $this->db->get($tabel);
		return $query->result();
	}

	function hapus($tabel, $id)
	{
		$this->db->where('id', $id);
		$this->db->delete($tabel);
	}

	function get_single_data($tabel, $id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($tabel);
		return $query->row();
	}

	function edit($tabel, $data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update($tabel, $data);
	}
}